FROM node:10

#COPY ./agar.io-clone /agar
RUN git clone https://github.com/huytd/agar.io-clone.git /agar

WORKDIR /agar

RUN npm install

CMD ["npm", "start"]

EXPOSE 3000
